import Vue from 'vue'
import Router from 'vue-router'


Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'layouts',
      component: () => import('./views/Layouts.vue')
    },
    {
      path: '/add-new-layout',
      name: 'AddLayout',
      component: () => import('./views/AddLayout.vue')
    }
  ]
})
