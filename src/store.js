import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    layouts: []
  },
  mutations: {
    ADD_NEW_LAYOUT(state, payload) {
      state.layouts.push(payload)
    }
  }
})
